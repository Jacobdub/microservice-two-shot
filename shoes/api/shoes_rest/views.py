from django.shortcuts import render
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Shoe, BinVO

from django.views.decorators.http import require_http_methods
import json



class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "closet_name", "bin_number", "bin_size"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["id", "name", "manufacturer", "color", "picture_url", "bin"]
    def get_extra_data(self, o):
        return {
            "bin": o.bin.closet_name,
        }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["name", "manufacturer", "color", "picture_url", "bin"]
    encoders = {"bin": BinVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )

    else: #POST
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            msg = f"Invalid bin id {bin_href}"
            return JsonResponse(
                {"message": msg},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

    else:
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
