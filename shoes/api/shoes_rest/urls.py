from django.urls import path
from .views import api_list_shoes, api_show_shoe

urlpatterns = [
    path("shoes/", api_list_shoes, name="shoe_list" ),
    path("shoes/<int:pk>/", api_show_shoe, name="show_shoe"),
]
