# Wardrobify

Team:

* Jacob Williams - shoes
* Mason McInerny - hats

## Design

## Shoes microservice
Created a Shoe model to store:
-Manufacturer
-Model
-Color
-Optional Picture
-Stored BIN (fetched from Wardrobe API)

Method        URL                     Description

GET        /api/shoes          Get a list of all stored shoes
GET      /api/shoes/{id}/       Get a single shoe by ID
POST       /api/shoes          Add a new shoe to database
PUT      /api/shoes{id}/        Update an existing shoe
DELETE   /api/shoes/{id}/       Delete a shoe by it's ID


## Hats microservice
I created The hat model to describe a specific hat.
I also created a LocationVO model that would utilize the Poller file to receive the data from the location Model in the wardrobe microservice.
The hat model when a post function was created would add a hat to the locationVO which would then create a location that
the server was able to use to display

Method           URL                            Description

GET         /api/locations/           Gets a list of all of the locations
GET      /api/locations/{id}/       Gets the details of one location
POST        /api/locations/         Creates a new location with the posted data
PUT      /api/locations/{id}/          Updates the details of one location
DELETE   /api/locations/{id}/              Deletes a single location
