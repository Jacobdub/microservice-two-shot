import { NavLink } from 'react-router-dom';

function ShoesList({shoes, getShoes}) {
    const deleteShoe = async (id) => {
    fetch(`http://localhost:8080/api/shoes/${id}/`, {
        method: "delete"
    })
    .then(() => {
        return getShoes()
        }).catch(console.log)
    }

    if(shoes === undefined) {
        return null;
    }

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Color</th>
                    <th>Image</th>
                    <th>Bin</th>
                    <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map((shoe) => {
                    return (
                        <tr key={shoe.id}>
                            <td>{ shoe.name }</td>
                            <td>{ shoe.manufacturer }</td>
                            <td>{ shoe.color }</td>
                            <td> <img src={shoe.picture_url} width="240" height="180" alt="shoe" /></td>
                            <td> { shoe.bin }</td>
                            <td>
                                <button onClick={() => deleteShoe(shoe.id)} type="button" className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                        )
                    })}
                </tbody>
            </table>
            <div>
                <NavLink className="nav-link" to="/shoes/new">Create new shoe</NavLink>
            </div>
        </div>
    );
}

export default ShoesList;
