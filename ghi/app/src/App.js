import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatForm from './HatForm';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import {useState, useEffect} from 'react'




function App() {
  const [shoes, setShoes] = useState([])
  const [hats, setHats] = useState([])
  const [bins, setBins] = useState([])
  const [locations, setLocations] = useState([])

  const getShoes = async () => {
    const shoeUrl = 'http://localhost:8080/api/shoes/'
    const shoeResponse = await fetch(shoeUrl);

    if (shoeResponse.ok) {
      const data = await shoeResponse.json();
      const shoes = data.shoes
      setShoes(shoes)
    }
  }
  const getHats = async () => {
    const hatUrl = 'http://localhost:8090/api/hats/'
    const hatResponse = await fetch(hatUrl);

    if (hatResponse.ok) {
      const data = await hatResponse.json();
      const hats = data.hats
      setHats(hats)
    }
  }
  const getBins = async () => {
    const shoeUrl = 'http://localhost:8100/api/bins/'
    const shoeResponse = await fetch(shoeUrl);

    if (shoeResponse.ok) {
      const data = await shoeResponse.json();
      const bins = data.bins
      setBins(bins)
    }
  }
  const getLocations = async () => {
    const hatUrl = 'http://localhost:8100/api/locations/'
    const hatResponse = await fetch(hatUrl);

    if (hatResponse.ok) {
      const data = await hatResponse.json();
      const locations = data.locations
      setLocations(locations)
    }
  }


  useEffect( () => {getShoes(); getBins(); getHats(); getLocations()}, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" >
            <Route path="" element={<HatsList hats={hats} getHats={getHats}/>} />
            <Route path="new" element={<HatForm locations={locations} getHats={getLocations} />} />
          </Route>
          <Route path="/shoes">
            <Route path=""element={<ShoesList shoes={shoes} getShoes={getShoes}/>} />
            <Route path="new" element={<ShoeForm bins={bins} getShoes={getShoes}/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
