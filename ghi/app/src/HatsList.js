import { NavLink } from 'react-router-dom';

function HatsList({hats, getHats}) {
    console.log(hats)
    const deleteHats = async (href) => {
    fetch(`http://localhost:8090${href}`, {
        method: "delete"
    })
    .then(() => {
        return getHats()
        }).catch(console.log)
    }

    if(hats === undefined) {
        return null;
    }


    return (
        <div>
        <table className="table table-striped">
            <thead>
                <tr>
                <th>Name</th>
                <th>fabric</th>
                <th>Color</th>
                <th>Style Name</th>
                <th>Picture</th>
                <th>Location</th>
                <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {hats.map((hat) => {
                return (
                    <tr key={hat.href}>
                        <td>{ hat.name }</td>
                        <td>{ hat.fabric }</td>
                        <td>{ hat.color }</td>
                        <td>{ hat.style_name }</td>
                        <td> <img src={hat.picture_url} width="240" height="180" alt="hat" /></td>
                        <td> { hat.location.closet_name }</td>
                        <td className= "align-middle">
                            <button onClick={() => deleteHats(hat.href)} type="button" className="btn btn-danger">Delete</button>
                        </td>
                    </tr>
                )
                })}
            </tbody>
        </table>
            <NavLink className="nav-link" to="/hats/new">Create new hat</NavLink>
        </div>
        );
    }

    export default HatsList;
