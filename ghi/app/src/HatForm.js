import React, {useEffect, useState} from 'react';


function HatForm( ) {
    const[locations, setLocations] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url)

        if (response.ok){
            const data = await response.json()
            setLocations(data.locations)
        }
    };


    const [name, setName] = useState('');
    const [fabric, setFabric] = useState('');
    const [color, setColor] = useState('');
    const [style_name, setStyleName] = useState('');
    const [picture, setPicture] = useState('');
    const [location, setLocation] = useState('');

    const handleNameChange = (e) => {
        const value = e.target.value;
        setName(value)
    }
    const handleFabricChange = (e) => {
        const value = e.target.value;
        setFabric(value)
    }
    const handleColorChange = (e) => {
        const value = e.target.value;
        setColor(value)
    }
    const handleStyleNameChange = (e) => {
        const value = e.target.value;
        setStyleName(value)
    }
    const handleLocationChange = (e) => {
        const value = e.target.value;
        setLocation(value)
    }
    const handlePictureChange = (e) => {
        const value = e.target.value;
        setPicture(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.fabric = fabric;
        data.color = color;
        data.style_name = style_name;
        data.picture_url = picture;
        data.location = location;

        console.log(data);

        const hatUrl = `http://localhost:8090/api/hats/`
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'applications/json'
            }
        }
        const response = await fetch(hatUrl, fetchConfig)
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat)
            setName('')
            setFabric('')
            setColor('')
            setStyleName('')
            setLocation('')
            setPicture('')
        }
    }

    useEffect(() => {
        fetchData();
        }, []);


    return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Hat</h1>
                <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                    <label htmlFor="manufacturer">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleStyleNameChange} value={style_name} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control"/>
                    <label htmlFor="style_name">Style Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePictureChange} value={picture} placeholder="Picture" required type="text" name="Picture" id="Picture" className="form-control"/>
                    <label htmlFor="Picture">Picture</label>
                </div>

                <div className="mb-3">
                    <select required onChange={handleLocationChange} name="location" id="location" className="form-select" value={location} >
                        <option value="">Choose a Location</option>
                        {locations.map(location => {
                            return (
                                <option key={location.id} value={location.href}>
                                    {location.closet_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    )};

export default HatForm;
